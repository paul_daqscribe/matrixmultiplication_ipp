

#include "stdafx.h"
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/multi_array.hpp>



#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "mkl.h"
#include "mkl_blas.h"

#include "C:\Users\DaqScribe\Desktop\examples\examples_core_c\cblas\source\mkl_example.h"
//#define SIZE 4
#define SIZE 2 * 2
#define min(x,y) (((x) < (y)) ? (x) : (y))

//#define ROW 512
//#define COL 512

using namespace std;
using namespace boost;

class CSampleThread
{

public:


	double *A, *B, *C;
	int m, n, k, i, j;
	double alpha, beta;

	vector <int> rowVector;
	vector <int> colVector;

	vector <double> m_vectorRow;
	vector <int> m_vectorCol;


	long long sumMatrix;
	long long multiplicationCount = 0;
	bool B_Start = true;

	unsigned long long second_keep;
	unsigned long long dur = 0;

	~CSampleThread();
	void InitValue();
	void TestThread();
	void TestThreadThird();

};

	CSampleThread::~CSampleThread()
	{
	}
	void CSampleThread::InitValue()
	{

		m = 1, k = SIZE, n = 1;
		printf(" Initializing data for matrix multiplication C=A*B for matrix \n"
			" A(%ix%i) and matrix B(%ix%i)\n\n", m, k, k, n);
		alpha = 1.0; beta = 0.0;

		/*printf(" Allocating memory for matrices aligned on 64-byte boundary for better \n"
			" performance \n\n");*/
		A = new double();
		B = new double();
		C = new double();
		/*A = (double *)mkl_malloc(m*k*sizeof(double), 64);
		B = (double *)mkl_malloc(k*n*sizeof(double), 64);
		C = (double *)mkl_malloc(m*n*sizeof(double), 64);*/
		if (A == NULL || B == NULL || C == NULL) {
			printf("\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
			free(A);
			free(B);
			free(C);
		}
		
		//for (int i = 0; i < SIZE; i++)
		//{
		//	m_vectorRow.push_back(i);
		//	m_vectorCol.push_back(i);
		//}

		//printf(" Intializing matrix data \n\n");
		for (i = 0; i < (m*k); i++) {
			A[i] = (double)(i + 1);
		}

		for (i = 0; i < (k*n); i++) {
			B[i] = (double)(-i - 1);
		}

		for (i = 0; i < (m*n); i++) {
			C[i] = 0.0;
		}

		//int xx = sizeof(m_vectorRow);
		//memcpy(A, &m_vectorRow, sizeof(m_vectorRow));
		//memcpy(B, &m_vectorRow, sizeof(m_vectorRow));


		/*for (i = 0; i < SIZE; i++) {
			printf("A : %d\n", A[i]);
			printf("B : %d\n", B[i]);
		}*/

		second_keep = clock();
		printf("Row Col size : %d\n", SIZE);

	}
	void CSampleThread::TestThread()
	{
		while (B_Start)
		{
			//printf(" Computing matrix product using Intel(R) MKL dgemm function via CBLAS interface \n\n");
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
				m, n, k, alpha, A, k, B, n, beta, C, n);
			//printf("\n Computations completed.\n\n");

			multiplicationCount++;



			//printf(" Top left corner of matrix A: \n");
			//for (i = 0; i<min(m, 6); i++) {
			//	for (j = 0; j<min(k, 6); j++) {
			//		printf("%12.0f", A[j + i*k]);
			//	}
			//	printf("\n");
			//}

			//printf("\n Top left corner of matrix B: \n");
			//for (i = 0; i<min(k, 6); i++) {
			//	for (j = 0; j<min(n, 6); j++) {
			//		printf("%12.0f", B[j + i*n]);
			//	}
			//	printf("\n");
			//}

			//printf("\n Top left corner of matrix C: \n");
			//for (i = 0; i<min(m, 6); i++) {
			//	for (j = 0; j<min(n, 6); j++) {
			//		printf("%12.5G", C[j + i*n]);
			//	}
			//	printf("\n");
			//}
			//B_Start = false;

		}
	}
	void CSampleThread::TestThreadThird()
	{
		while (B_Start)
		{
			dur = clock() - second_keep;
			if (dur > 1000)
			{
				printf("Count : %d\n", multiplicationCount);
				multiplicationCount = 0;
				second_keep += dur;

				if (_kbhit())
				{
					B_Start = false;
					free(A);
					free(B);
					free(C);
					/*mkl_free(A);
					mkl_free(B);
					mkl_free(C);*/
				}
			}
		}
	}


int main(int argc, char* argv[])
{

	//CSampleThread Cthread;
	//Cthread.InitValue();

	//thread th1 = thread(bind(&CSampleThread::TestThread, &Cthread));
	//thread th3 = thread(bind(&CSampleThread::TestThreadThird, &Cthread));

	//th1.join();


	//int AA[5];
	//short BB[10];
	//int xx = sizeof(AA);
	//int yy = sizeof(BB);
	//memset(AA, 1, sizeof(AA));
	//memcpy(BB, AA, sizeof(BB));
	//for (int i = 0; i < 20; i++)
	//	cout << BB[i] << endl;





	unsigned long long dur = 0;
	unsigned long long second_keep = 0;
	long long multiplicationCount = 0;

	double *A, *B, *C;
	int m, n, k, i, j;
	double alpha, beta;

	printf("\n This example computes real matrix C=alpha*A*B+beta*C using \n"
		" Intel(R) MKL function dgemm, where A, B, and  C are matrices and \n"
		" alpha and beta are double precision scalars\n\n");

	m = 1, k = 2, n = SIZE;
	printf(" Initializing data for matrix multiplication C=A*B for matrix \n"
		" A(%ix%i) and matrix B(%ix%i)\n\n", m, k, k, n);
	alpha = 1.0; beta = 0.0;

	printf(" Allocating memory for matrices aligned on 64-byte boundary for better \n"
		" performance \n\n");
	A = (double *)mkl_malloc(m*k*sizeof(double), 64);
	B = (double *)mkl_malloc(k*n*sizeof(double), 64);
	C = (double *)mkl_malloc(m*n*sizeof(double), 64);
	if (A == NULL || B == NULL || C == NULL) {
		printf("\n ERROR: Can't allocate memory for matrices. Aborting... \n\n");
		mkl_free(A);
		mkl_free(B);
		mkl_free(C);
		return 1;
	}

	printf(" Intializing matrix data \n\n");
	for (i = 0; i < (m*k); i++) 
	{
		//kjk constant
		if (i + 1 >= k)
		{
			A[i] = 5;
		}
		else
		{
			A[i] = (double)(i + 1 + 1);
		}
	}

	for (i = 0; i < (k*n); i++) 
	{
		//kjk constant
		if (i >= n)
		{
			B[i] = 1;
		}
		else
		{
			B[i] = (double)(i + 1);
		}
	}

	for (i = 0; i < (m*n); i++) {
		C[i] = 0.0;
	}

	while (true)
	{
		//printf(" Computing matrix product using Intel(R) MKL dgemm function via CBLAS interface \n\n");
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
			m, n, k, alpha, A, k, B, n, beta, C, n);
		//printf("\n Computations completed.\n\n");
		multiplicationCount++;


		dur = clock() - second_keep;
		if (dur > 1000)
		{
			printf("Count : %d\n", multiplicationCount);
			multiplicationCount = 0;
			second_keep += dur;

		}

		printf(" Top left corner of matrix A: \n");
		for (i = 0; i<min(m, 6); i++) {
			for (j = 0; j<min(k, 6); j++) {
				printf("%12.0f", A[j + i*k]);
			}
			printf("\n");
		}

		printf("\n Top left corner of matrix B: \n");
		for (i = 0; i<min(k, 6); i++) {
			for (j = 0; j<min(n, 6); j++) {
				printf("%12.0f", B[j + i*n]);
			}
			printf("\n");
		}

		printf("\n Top left corner of matrix C: \n");
		for (i = 0; i<min(m, 6); i++) {
			for (j = 0; j<min(n, 6); j++) {
				printf("%12.5G", C[j + i*n]);
			}
			printf("\n");
		}

		//return 0;
	}

	

	printf("\n Deallocating memory \n\n");
	mkl_free(A);
	mkl_free(B);
	mkl_free(C);

	printf(" Example completed. \n\n");



	return 0;
	
}